var controller, groupView, personView, group, previewView,
    mediator = new Mediator();

$(function () {
  group = new Group();
  group.getData();

  groupView = new GroupView({el: $('#personalInfo')});
  personView = new PersonView({el: $('.shortView')});
  previewView = new PreviewView({el: $('.previewTab')});

  controller = new Controller();

  controller.triggerSubscribes();
});


