var http = require('http'),
    fs = require('fs'),
    data;

fs.readFile("text.json", "binary", function(error, file) {
  if(!error) {
    data = JSON.parse(file.slice(3));
  }
});


function onRequest (request, response) {
  var path = request.url,
      pathId = urlParse(path),
      postData = '';

  function urlParse (url) {
    if (url.substr(0,8) === '/getData') {
      path = '/getData';
    }

    return url.slice(9-url.length);
  }

  console.log(path);

  switch(path) {
    case '/':
      fs.readFile("../JS Mod.4.html", "binary", function(error, file) {
        if(error) {
          response.writeHead(404, {"Content-Type": "text/plain"});
          response.write('404 Index.html Not Found' + "\n");
          response.end();
        } else {
          response.writeHead(200, {"Content-Type": "text/html"});
          response.write(file, "binary");
          response.end();
        }
      });
      break;

    case '/getData':
      console.log(request.method);

      switch(request.method) {
        case 'GET':
          response.writeHead(200, {"Content-Type": "text/json"});
          response.write(JSON.stringify(data), "binary");
          response.end();
          break;

        case 'POST':
        case 'PUT':
          request.addListener("data", function(postDataChunk) {
            postData += postDataChunk;
          });

          request.addListener("end", function() {
            postData = JSON.parse(postData);
            if (pathId) {
              data[pathId-1] = postData;
            }
          });
          console.log('Person['+ pathId + '] has been changed!');
          break;

        case 'DELETE':
          delete data[pathId-1];
          //data.splice(pathId-1, 1);
          response.writeHead(200, {"Content-Type": "text/plain"});
          response.write('Person['+ pathId + '] has been deleted!', "binary");
          response.end();
          console.log('Person['+ pathId + '] has been deleted!');
          break;
      }
      break;

    case '/JS3CSS.css':
      fs.readFile('../JS3CSS.css', "binary", function(error, file) {
        if(error) {
          response.writeHead(404, {"Content-Type": "text/text"});
          response.write('404 text.txt Not Found');
          response.end();
        } else {
          response.writeHead(200, {"Content-Type": "text/css"});
          response.write(file, "binary");
          response.end();
        }
      });
      break;

    default:
      fs.readFile('../' + path, "binary", function(error, file) {
        if(error) {
          response.writeHead(404, {"Content-Type": "text/plain"});
          response.write(path + ' Not Found');
          console.log(path + ' Not Found');
          response.end();
        } else {
          response.writeHead(200, {"Content-Type": "text/plain"});
          response.write(file, "binary");
          response.end();
        }
      });
      break;
  }
}

http.createServer(onRequest).listen(8888);
