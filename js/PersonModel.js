var Person = Backbone.Model.extend({

  defaults: {
    "name" : '',
    "surname" : '',
    "middleName" : '',
    "passport" : '',
    "taxNumber" : '',
    "gender" : '',
    "age": ''
  }
});