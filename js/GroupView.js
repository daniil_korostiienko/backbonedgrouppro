var GroupView = Backbone.View.extend({
  templateGroupPro: _.template('<div class="selectorTab">' +
    '</div>' +
    '<div class="shortView view hidden">' +
    '</div>' +
    '<div class="previewTab hidden">' +
    '</div>'),

  groupMemberView: [],

	events: {
		'click .createButton': 'addNewPerson'
	},
	
	addNewPerson: function () {
		mediator.publish('personCreationRequested', null);
	},

  initialize: function () {
    this.$el.html(this.templateGroupPro);
    this.refreshList();
  },

  refreshList: function () {
    var template = _.template('<ul class="personList"></ul><button class="serviceButtons createButton">Create New Person</button>');

    $(".selectorTab").html(template);

    _.forEach(group, function (value, index) {
      if (group.at(index).get('id') !== undefined) {
        $(".selectorTab .personList").append(new GroupMemberView({model: group.at(index)}).render());
      }
	  }, this);
  }
});