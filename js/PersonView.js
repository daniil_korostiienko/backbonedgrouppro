var PersonView = Backbone.View.extend({
  activePerson: 0,
  $tabs: '',
  $inputs: '',
  $buttons: '',
  model: '',

	events: {
		'click .saveButton': 'savePersonB',
		'click .previewButton': 'showPreview',
		'click .hideAllButton': 'hideAll',
		'click .deleteButton': 'deletePersonB'
	},
	
	savePersonB: function () {
		mediator.publish('savePerson: requested');
	},
	
	showPreview: function () {
		mediator.publish('showPreview: requested', this.model.id);
	},
	
	hideAll: function () {
		mediator.publish('hidePreview: requested');
		mediator.publish('hidePerson');
	},
	
	deletePersonB: function () {
      mediator.publish('deletePerson', this.model.id);
	},
	
  initialize: function () {
    var templateSwitcherTab = _.template('<div class="switcherTab">' +
          '<button class="button">Names</button>' +
          '<button class="button">Passport</button>' +
          '<button class="button">Gender/Age</button>' +
          '<button class="serviceButtons saveButton">Save</button>' +
          '<button class="serviceButtons previewButton">Preview</button>' +
          '<button class="serviceButtons hideAllButton">Hide</button>' +
          '<button class="serviceButtons deleteButton">Delete</button>' +
          '</div>'),

        templateTabs = _.template('<div class="tab">' +
          '<p>Name</p>' +
          '<input class="input" name="name"></input>' +
          '<p>Surname</p>' +
          '<input class="input" name="surname"></input>' +
          '<p>Middle name</p>' +
          '<input class="input" name="middleName" value="123"></input>' +
          '</div>' +
          '<div class="tab hidden">' +
          '<p>Passport</p>' +
          '<input class="input" name="passport"></input>' +
          '<p>TaxNumber</p>' +
          '<input class="input" name="taxNumber"></input>' +
          '</div>' +
          '<div class="tab hidden">' +
          '<p>Gender</p>' +
          '<input class="input" name="gender"></input>' +
          '<p>Age</p>' +
          '<input class="input" name="age"></input>' +
          '</div>');

    this.$el.html(templateSwitcherTab());
    this.$el.append(templateTabs());
    this.$tabs = this.$('.tab');  // because this.$ here = this.$el.find
    this.$inputs = this.$('.input');
    this.$buttons = this.$('.button');
	
	
	
	_.forEach(this.$buttons, function (button, index) { // to events
      $(button).click(function () {
          manageAll.call(this, index);
      }.bind(this));
    }, this);
    
    function manageAll (i) {
      _.forEach(this.$tabs, function(tab, index) {
        $(tab).addClass('hidden');
      }, this);
      this.$tabs.eq(i).removeClass('hidden');
    }
  },

  showPerson: function (model) {
    this.model = model;//group.getStudent(personId);

    //this.activePerson = Number(personId);
    personView.initialize();
    console.log(this.model);

    mediator.publish('hidePreview: requested');
    this.$el.removeClass('hidden');

    _.forEach(this.model.attributes, function (attribute, index) {
      if (attribute !== '') {
		this.$("[class = 'input'][name = '" + index + "']").val(attribute);
      } else {
		this.$("[class = 'input'][name = '" + index + "']").val('');
      }
    }, personView);
  },

  savePerson: function () {
    var attributes = {
        "name" : '',
        "surname" : '',
        "middleName" : '',
        "passport" : '',
        "taxNumber" : '',
        "gender" : '',
        "age": ''
      };

    _.forEach(attributes, function (attrValue, key) {
      attributes[key] = $("[class = 'input'][name = '" + key + "']").val();
    });

    this.model.set(attributes);
    mediator.publish('refreshPersonInGroupList', this.model.id);
  },

  hidePerson: function () {
    this.$el.addClass('hidden');
    mediator.publish('hidePreview: requested');
  },

  deletePerson: function () {
    group.deleteStudent(this.model.id);
    mediator.publish('hidePerson');
    mediator.publish('hidePreview');
    mediator.publish('personDeleted', this.model.id);
  }
});