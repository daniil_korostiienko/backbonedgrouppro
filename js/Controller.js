function Controller () {
  this.triggerSubscribes = function () {
    mediator.subscribe('personCreated', groupView.refreshList, null, groupView);
    mediator.subscribe('personDeleted', groupView.refreshList, null, groupView);
	  mediator.subscribe('refreshPersonInGroupList', groupView.refreshList, null, groupView);

    mediator.subscribe('personCreationRequested', group.addStudent, null, group);  //wrong names everywhere

    _.forEach(groupView.groupMemberView, function (value, index) {
		  mediator.subscribe('personChanged', value.refreshPersonInList);
      mediator.subscribe('personDeleted', value.remove);
	  });

    mediator.subscribe('personViewed', personView.showPerson, null, personView);
    mediator.subscribe('savePerson: requested', personView.savePerson, null, personView);
    mediator.subscribe('requestActivePerson', personView.getActivePerson, null, personView);
    mediator.subscribe('hidePerson', personView.hidePerson, null, personView);
    mediator.subscribe('deletePerson', personView.deletePerson, null, personView);

    mediator.subscribe('showPreview: requested', previewView.showPreview, null, previewView);
    mediator.subscribe('hidePreview: requested', previewView.hidePreview, null, previewView);
    mediator.subscribe('activePerson', previewView.updateActivePerson, null, previewView);
  }
}