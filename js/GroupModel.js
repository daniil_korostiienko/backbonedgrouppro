var Group = Backbone.Collection.extend({

  model: Person,

  url: "/getData",

  addStudent: function (person, key) {
    if (!person) {
      person = {"name":"New", "surname": "Student"};
    }

    person['id'] = group.length;
    this.add(person);

    this.at(group.length).save(person);

    mediator.publish('personCreated');
  },

  getStudent: function (studentId) {  //get student's attributes
    return this.at(studentId).attributes;
  },


  setStudent: function (activePerson, attributesHash) {  //get student's attributes
    this.at(activePerson).set(attributesHash);

    this.at(activePerson).save(attributesHash);
  },

  getData: function () {
    this.fetch({success: function (collection, response, options) {
      mediator.publish('personCreated');
      console.log(response);
      }
    });
  },

  deleteStudent: function (studentId) {
    this.at(studentId).destroy({success: function (model, response, options) {
      //this.models.splice(studentId, 1);

      console.log(response.responseText)
    }
    });
  }
});

var data = {
  "1234": {
    "name": "Bogdan",
    "surname": "Marchenko",
    "age": 18,
    "taxNumber": 1234
  },
  "2345": {
    "name": "Olga",
    "surname": "Shafarenko",
    "age": 27,
    "taxNumber": 2345
  },
  "3456": {
    "name": "Katya",
    "surname": "Ovcharenko",
    "age": 22,
    "taxNumber": 3456
  },
  "4567": {
    "name": "Egor",
    "surname": "Duhov",
    "taxNumber": 4567
  },
  "5678": {
    "name": "Daniil",
    "surname": "Korostiienko",
    "age": 21,
    "taxNumber": 5678
  },
  "6789": {
    "name": "Sergey",
    "surname": "Miroshnik",
    "age": 22,
    "taxNumber": 6789
  }
};