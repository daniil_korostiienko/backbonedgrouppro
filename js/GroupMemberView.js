var GroupMemberView = Backbone.View.extend({
  template: _.template('<li class="personListElement"><%= name %> <%= surname %></li>'),

  model: '',

  events: {
    'click': 'showPerson',
    'contextmenu': 'previewPerson'
  },

  initialize: function (input) {
    this.model = input.model;
  },
  
  showPerson: function () {
    mediator.publish('personViewed', this.model);
  },

  previewPerson: function (e) {
    e.preventDefault();
    mediator.publish('showPreview: requested', this.model.id);
  },

  render: function () {
    this.$el.html(this.template({name: this.model.get('name'), surname: this.model.get('surname')[0]}));
    return this.$el;
  }
});