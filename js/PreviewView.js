var PreviewView = Backbone.View.extend({
  activePerson: 0,

  initialize: function () {

  },
  
  events: {
	  'click .hidePreviewButton': 'hidePreview'
  },
  
  hidePreview: function () {
	mediator.publish('hidePreview: requested');
  },

  showPreview: function (personId) {
    var attributes = {
          "name" : 'Name',
          "surname" : 'Surname',
          "middleName" : 'MiddleName',
          "passport" : 'Passport',
          "taxNumber" : 'TaxNumber',
          "gender" : 'Gender',
          "age": 'Age'
        },
      templateList  = _.template('<ul class="preview"></ul><button class="serviceButtons hidePreviewButton">Close</button>'),
      templateListElement  = _.template('<li><%= attribute %>: <%= value %></li>');

    this.$el.html(templateList());
    this.$el.removeClass('hidden');

    if (personId) {
      this.activePerson = personId;
    } else {
      mediator.publish('requestActivePerson');
    }

    _.forEach(attributes, function (attrValue, attrKey) {
      $('.preview').append(templateListElement({attribute: attrValue, value: group.getStudent(this.activePerson)[attrKey]}));
    }, this);

  },

  updateActivePerson: function (value) {
    this.activePerson = value;
  },

  hidePreview: function () {
    this.$el.addClass('hidden');
  }
});